---
layout: markdown_page
title: "Diversity, Inclusion & Belonging Sharing Page"
description: "This page provides a place for team members to share any learnings for Ally Learning Groups, DIB Rountables and other DIB initiatives."
canonical_path: "/company/culture/inclusion/DIB-sharing-page/"
---

## Diversity, Inclusion & Belonging Sharing Page 

### Privilege For Sale Sharing 

**Example:**
- Group Members: 
- Privileges Chosen: 
- Reason for Privileges Chosen:
- Learnings from the experience: 


- Group Members: Hannah Schuler, Julia Hill- Wright, Lea Tuizat, Ryan Kimball and Steve Cull
- Privileges Chosen: 2,8,11,20,5,14,18,16,3,7
- Reason for Privileges Chosen:
- Learnings from the experience:

### DIB Roundtable Sharing

**Example:**
- Group Members:
- What is one thing as a group can we do to make a positive impact on Diversity, Inclusion and Belonging?:
- What did the group learn from the experience?: 

- Group Members: Hannah Schuler, Julia Hill- Wright, Lea Tuizat, Ryan Kimball, Steve Cull, Shawn Winters, Kaleb Hill
- What is one thing as a group can we do to make a positive impact on Diversity, Inclusion and Belonging?:
1. System of communication to be more honest and candid
1. Hold space for one another and be really, crowd source help and not feel ashamed. 
1. Being empathetic to everyone’s situations, where they came from before being here. Challenging bias.
1. Safe space to talk through tough situations. Not making assumptions. approaching with kindness empathy and respect.
1. Self accountability, do your part to make people feel a part of the team and welcome. 
1. Empathy first. 
1. Asking better questions of other people. Help me understand where other people came from. 

- What did the group learn from the experience?: 

### Ally Lab Learning Groups Activity Sharing 

**Example:**
- Group Members:
- Activity:
- Learnings, Actions, or Answers etc:  

### Ally Lab Learning Groups Committment and Values pledge Sharing

**Example:**
- Group Members
- Committment: As a group we commit to 
- Values: The values we have chosen to ensure we align to out committment are as follows: Example: Action: We will say something when we see a DIB issue
